/**************************************
    MAPA
*************************************/

function initialize() {

	// Exibir mapa;
	var myLatlng = new google.maps.LatLng(-23.1796902,-47.2630103);
	var mapOptions = {
	  zoom: 17,
	  center: myLatlng,
	  panControl: false,
	  // mapTypeId: google.maps.MapTypeId.ROADMAP
	  mapTypeControlOptions: {
		mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
	  }
	}
  
  
	// Parâmetros do texto que será exibido no clique;
	var contentString = '<h2>Green Park II</h2>' +
	'<p>Av. Remigio Dalla Vecchia, 688, Salto - SP - CEP 13326-110</p>' +
	'<a href="https://goo.gl/maps/wVF14qdpwrv" target="_blank">clique aqui para mais informações</a>';
	var infowindow = new google.maps.InfoWindow({
		content: contentString,
		maxWidth: 700
	});
  
  
	// Exibir o mapa na div #mapa;
	var map = new google.maps.Map(document.getElementById("map"), mapOptions);
  
  
	// Marcador personalizado;
	var image = 'assets/img/map_icon.png';
	var marcadorPersonalizado = new google.maps.Marker({
		position: myLatlng,
		map: map,
		icon: image,
		title: 'Green Park',
		animation: google.maps.Animation.DROP
	});
  
  
  //   // Exibir texto ao clicar no ícone;
	google.maps.event.addListener(marcadorPersonalizado, 'click', function() {
	  infowindow.open(map,marcadorPersonalizado);
	});
  
  
	
}
  
  
  // Função para carregamento assíncrono
  function loadScript() {
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyDmjkEri5pEh98mYRj8Jq9b4XqK3LoU_6w&sensor=true&callback=initialize";
	document.body.appendChild(script);
}
  
  window.onload = loadScript;
  