
<section id="banner">
  <div class="text"><img src="assets/img/leaf.png"/>
    <p>Apartamentos com 2 dormitórios (1 suíte)<span>ÓTIMA OPORTUNIDADE PARA MORAR OU INVESTIR!</span></p>
  </div>
</section>
<header>
  <div class="border"><span class="span1"></span><span class="span2"></span><span class="span3"></span><span class="span4"></span><span class="span5"></span><span class="span6"></span><span class="span7"></span><span class="span8"></span><span class="span9"></span></div>
  <div class="wrap"><img src="assets/img/logo.png" class="logo"/>
    <button><span class="stripe"></span><span class="stripe"></span><span class="stripe"></span></button>
    <nav><a data-section="#differentials">DIFERENCIAIS</a><span></span><a data-section="#greenpark">GREENPARK</a><span></span><a data-section="#blueprint">APARTAMENTOS</a><span></span><a data-section="#video">VÍDEO</a><span></span><a data-section="#location">LOCALIZAÇÃO</a><span></span><a data-section="#contact">VENDAS</a><span></span><a data-section="#work-progress">OBRAS</a>
    </nav>
  </div>
</header>
<section id="differentials">
  <div class="wrap">
    <p class="title">Faz bem viver bem.</p>
    <p>Nenhum outro empreendimento é tão completo como o Residencial GreenPark. </p>
  </div>
  <div class="icons">
    <div class="wrap">
      <figure>
        <div class="img icon-1"></div>
        <figcaption>VARANDA GOURMET C/ VISTA PRIVILEGIADA</figcaption>
      </figure>
      <figure>
        <div class="img icon-2"></div>
        <figcaption>GARAGEM COBERTA OU PROTEGIDA DO SOL</figcaption>
      </figure>
      <figure>
        <div class="img icon-3"></div>
        <figcaption>ÁREA DE LAZER PRIVATIVA C/ PISCINA</figcaption>
      </figure>
      <figure>
        <div class="img icon-4"></div>
        <figcaption>INFRAESTRUTURA P/ AR CONDICIONADO</figcaption>
      </figure>
      <figure>
        <div class="img icon-5"></div>
        <figcaption>BICICLETÁRIO</figcaption>
      </figure>
    </div>
  </div>
</section>
<section id="greenpark">
  <div class="wrap">
    <h2 class="title"> <b>GREENPARK </b>é cercado por muito verde, ar puro e tranquilidade.</h2>
    <div class="text">
      <p>GreenPark é um residencial vertical completo, cercado por muito verde, ar puro e tranquilidade. Está localizado perto do centro e ao mesmo tempo longe do barulho e da poluição da cidade. São 88 apartamentos de 65 a 74m2, com  garagem coberta e varanda gourmet. Um excelente lugar para morar ou para investir. </p>
      <p>GreenPark é um empreendimento da TeamPrime Empreendimentos. Mais informações ou tabela de preços: FALE CONOSCO.</p>
    </div>
    <figure><img data-number="1" src="assets/img/modal-banner-img-01-thumb.png" alt=""/><img data-number="2" src="assets/img/modal-banner-img-02-thumb.png" alt=""/><img data-number="3" src="assets/img/modal-banner-img-03-thumb.png" alt=""/><img data-number="4" src="assets/img/modal-banner-img-04-thumb.png" alt=""/><img data-number="5" src="assets/img/modal-banner-img-05-thumb.png" alt=""/><img data-number="6" src="assets/img/modal-banner-img-06-thumb.png" alt=""/></figure>
    <div class="text">
      <p>Morar perto do centro sem ter os transtornos diários do trânsito, poluição e barulho. GreenPark, um bom lugar para morar ou investir.</p>
    </div>
  </div>
</section>
<section id="blueprint">
  <div class="wrap">
    <div class="nav"><a data-number="1" class="left active">PLANTA APTO 65M²</a><a data-number="2" class="right">PLANTA APTO 74M²</a></div>
    <figure><img data-number="1" src="assets/img/blueprint-01.png" alt="Planta baixa do Apartamento de 65m² no Condominio Residencial Greenpark em Salto, interior de São Paulo" class="active"/><img data-number="2" src="assets/img/blueprint-02.png" alt="Planta baixa do Apartamento de 74m² no Condominio Residencial Greenpark em Salto, interior de São Paulo"/>
      <button class="ctrl left"></button>
      <button class="ctrl right"></button>
      <figcaption>Imagens ilustrativas do Residencial GreenPark</figcaption>
    </figure><img src="assets/img/logo-teamprime.png" class="logo"/>
  </div>
</section>
<section id="video">
  <div class="wrap">
    <div class="video">
      <iframe src="https://www.youtube.com/embed/RnqSofKI5mc" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
    </div>
  </div>
</section>
<section id="location">
  <div class="wrap">
    <p class="title">O Residencial GreenPark está localizado à 4 minutos do centro da cidade de Salto. </p>
    <div class="address"><img src="assets/img/location.png"/>
      <p>RUA REMIGIO DALLA VECCHIA, 431 JARDIM NAIR MARIA - SALTO</p>
    </div>
  </div>
  <div id="map"></div>
</section>
<section id="contact">
  <div class="wrap">
    <p>Para mais informações ou valores, fale conosco. Estamos a disposição.</p>
    <form data-form="#contact_form" onsubmit="send_form(this); egoi(this);  return false" id="contact_form">
      <div class="col1">
        <div class="textbox">
          <input type="text" id="name" name="name" required="required" class="text name"/>
          <label for="name">Nome</label>
        </div>
        <div class="textbox">
          <input type="email" id="email" name="email" data-form="#contact_form" required="required" class="text email"/>
          <label for="email">Email</label>
        </div>
        <div class="textbox">
          <input type="tel" id="phone" name="phone" class="text phone"/>
          <label for="phone">Telefone</label>
        </div>
      </div>
      <div class="col2">
        <div class="textbox">
          <textarea id="msg" name="msg" class="text msg"></textarea>
          <label for="msg">Mensagem</label>
        </div>
        <button class="submit">enviar</button>
      </div>
    </form>
    <div class="contact-details">
      <div class="col1 online"><img src="assets/img/mail.png"/>
        <div class="text">
          <p class="bold">vendas / informações</p><a href="mailto:contato@teamprime.com.br">contato@teamprime.com.br</a><a href="http://www.teamprime.com.br">www.teamprime.com.br</a>
        </div>
      </div>
      <div class="col1 offline"><img src="assets/img/phone.png"/>
        <div class="text">
          <p class="bold">vendas / informações</p><a href="tel:1140280805">(11) 4028.0805</a><a href="tel:1128401824">(11) 2840.1824</a>
        </div>
      </div>
      <div class="col1 registration-form"><img src="assets/img/registration.png"/>
        <div class="text">
          <p class="bold">Ficha Cadastral</p><a href="ficha-cadastral" target="_blank">Clique aqui e faça seu cadastro</a>
        </div>
      </div>
    </div><img src="assets/img/dad-and-son.png" class="img"/>
    <div class="footnote"><img src="assets/img/logo-greenpark-footer.png" class="greenpark"/>
      <p>GreenPark é um empreendimento idealizado e realizado pela empresa: Team Prime Empreendimentos.</p><img src="assets/img/logo-teamprime.png" class="teamprime"/>
    </div>
  </div>
</section>
<footer id="work-progress">
  <div class="wrap"><img src="assets/img/work-progress.png" class="img"/>
    <div class="text">
      <p class="title">Imagem da Obra Atualizada em: <span class="date"><?php require 'admin/update.txt';?></span></p>
      <p>A imagem ao lado é atualizada a cada <span class="br"></span>15 dias para que você acompanhe a obra em tempo real.<span class="br"></span>Mais um diferencial Team Prime Empreendimentos.</p><img src="assets/img/logo-teamprime.png"/>
    </div>
  </div>
</footer>
<div class="modal background"></div>
<div class="modal banner">
  <div class="close-button">&times;</div><img data-number="1" src="assets/img/modal-banner-img-01-480.jpg" srcset="assets/img/modal-banner-img-01-960.jpg 2x, assets/img/modal-banner-img-01-480.jpg 480w, assets/img/modal-banner-img-01-1280.jpg 960w" alt="" class="active"/><img data-number="2" src="assets/img/modal-banner-img-02-480.jpg" srcset="assets/img/modal-banner-img-02-960.jpg 2x, assets/img/modal-banner-img-02-480.jpg 480w, assets/img/modal-banner-img-02-1280.jpg 960w" alt=""/><img data-number="3" src="assets/img/modal-banner-img-03-480.jpg" srcset="assets/img/modal-banner-img-03-960.jpg 2x, assets/img/modal-banner-img-03-480.jpg 480w, assets/img/modal-banner-img-03-1280.jpg 960w" alt=""/><img data-number="4" src="assets/img/modal-banner-img-04-480.jpg" srcset="assets/img/modal-banner-img-04-960.jpg 2x, assets/img/modal-banner-img-04-480.jpg 480w, assets/img/modal-banner-img-04-1280.jpg 960w" alt=""/><img data-number="5" src="assets/img/modal-banner-img-05-480.jpg" srcset="assets/img/modal-banner-img-05-960.jpg 2x, assets/img/modal-banner-img-05-480.jpg 480w, assets/img/modal-banner-img-05-1280.jpg 960w" alt=""/><img data-number="6" src="assets/img/modal-banner-img-06-480.jpg" srcset="assets/img/modal-banner-img-06-960.jpg 2x, assets/img/modal-banner-img-06-480.jpg 480w, assets/img/modal-banner-img-06-1280.jpg 960w" alt=""/>
  <div class="thumbs">
    <button data-number="1" id="thumb1" class="active"></button>
    <button data-number="2" id="thumb2"></button>
    <button data-number="3" id="thumb3"></button>
    <button data-number="4" id="thumb4"></button>
    <button data-number="5" id="thumb5"></button>
    <button data-number="6" id="thumb6"></button>
  </div>
  <button title="Clique para ver a imagem anterior" class="ctrl left"></button>
  <button title="Clique para ver a próxima imagem" class="ctrl right"></button>
</div>
<div data-displayed="false" class="modal form">
  <div class="title"> 
    <p class="bold">A Deltalog liga pra você</p>
    <p>Preencha as informações abaixo e nosso consultor entrará em contato</p>
  </div>
  <div class="shake_title">
    <p class="bold">Só um minuto</p>
    <p>Gostaria de receber mais informações sem compromisso? Preencha as informações abaixo e nosso consultor entrará em contato</p>
  </div>
  <form data-form="#modal_form" onsubmit="send_form(this); egoi(this);  return false" id="modal_form">
    <div>
      <input type="text" id="name2" name="name" required="required" class="text name"/>
      <label for="name2">Nome</label>
    </div>
    <div>
      <input type="email" id="email2" name="email" data-form="#modal_form" required="required" class="text email"/>
      <label for="email2">Email</label>
    </div>
    <div>
      <input type="tel" id="phone2" name="phone" required="required" class="text phone"/>
      <label for="phone2">Telefone</label>
    </div>
    <div>
      <textarea id="msg2" name="msg" required="required" class="text msg"></textarea>
      <label for="msg2">Mensagem</label>
    </div>
    <div>
      <button class="submit">enviar</button>
    </div>
  </form>
</div>