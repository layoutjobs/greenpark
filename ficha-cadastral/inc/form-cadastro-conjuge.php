<br>
<label class="w2">
	Nome*
	<input id="nome_conjuge" name="nome_conjuge" required>
</label>

<label class="w6">
	Nascimento*
	<input type="date" name="nascimento_conjuge" id="nascimento_conjuge" required>
</label>

<label class="w6">
	Naturalidade*
	<input id="naturalidade_conjuge" name="naturalidade_conjuge" required>
</label>

<label class="w6">
	Nacionalidade*
	<input id="nacionalidade_conjuge" name="nacionalidade_conjuge" required>
</label>

<label class="w6">
	Sexo*
	<select id="sexo_conjuge" name="sexo_conjuge" required>
		<option></option>
		<option value="Masculino">Masculino</option>
		<option value="Feminino">Feminino</option>
	</select>
</label>

<label class="w6">
	RG / RNE / PASS*
	<input id="rg_rne_pass_conjuge" name="rg_rne_pass_conjuge" required>
</label>

<label class="w6">
	CPF*
	<input id="cpf_conjuge" name="cpf_conjuge" placeholder="123.456.789-10" required>
</label>

<label class="w6">
	Empresa*
	<input id="empresa_conjuge" name="empresa_conjuge" required>
</label>

<label class="w6">
	Profissão*
	<input id="profissao_conjuge" name="profissao_conjuge" required>
</label>

<label class="w6">
	Tempo de Serviço*
	<input type="number" id="tempo_de_servico_conjuge" name="tempo_de_servico_conjuge" min="0" data-affixes-stay="true" data-prefix="" data-thousands="." data-decimal="" placeholder="0" required>
</label>

<label class="w4">
	Renda*
	<input id="renda_conjuge" name="renda_conjuge" data-affixes-stay="true" data-prefix="R$ " data-thousands="." data-decimal="," placeholder="R$ 0,00" required>
</label>

<label class="w4">
	FGTS*
	<input id="fgts_conjuge" name="fgts_conjuge" data-affixes-stay="true" data-prefix="R$ " data-thousands="." data-decimal="," placeholder="R$ 0,00">
</label>

<label class="w4">
	Outras Rendas
	<input id="outras_rendas_conjuge" name="outras_rendas_conjuge" data-affixes-stay="true" data-prefix="R$ " data-thousands="." data-decimal="," placeholder="R$ 0,00">
</label>

<label class="w4">
	Celular*
	<input id="celular_conjuge" name="celular_conjuge" placeholder="11-1234-56789" required>
</label>

<hr>
<br><br>