<form action='inc/sql-insert-data.php' method='post'>
    <img src='img/logo-greenpark.png' style='float:left'>
    <img src='img/logo-teamprime.png' style='float:right'>


    <div id='cadastro'>
        <h2>Ficha Cadastral - Pessoa Física</h2>

        
        <label class='w2'>
            Nome*
            <input id='nome' name='nome' required>
        </label>

        
        <label class='w6'>
            Sexo*
            <select id='sexo' name='sexo' required>
                <option></option>
                <option value='Masculino'>Masculino</option>
                <option value='Feminino'>Feminino</option>
            </select>
        </label>

        
        <label class='w6'>
            Nascimento*
            <input type='date' name='nascimento' required>
        </label>
        

        <label class='w6'>
            Naturalidade*
            <input id='naturalidade' name='naturalidade' required>
        </label>


        <label class='w5'>
            Nacionalidade*
            <input id='nacionalidade' name='nacionalidade' required>
        </label>
        

        <label class='w5'>
            Estado Civil*
            <select id='estado_civil' name='estado_civil' required>
                <option></option>
                <option value='solteiro'>Solteiro</option>
                <option value='casado'>Casado</option>
                <option value='divorciado'>Divorciado</option>
                <option value='viuvo'>Viuvo</option>
            </select>
        </label>


        <label class='w5'>
            Regime de Casamento*
            
            <select id='regime_casamento' name='regime_casamento' required>
                <option></option>
                <option value='total'>Total</option>
                <option value='parcial'>Parcial</option>
                <option value='separacao'>Separação</option>

            </select>
        </label>


        <label class='w5'>
            RG / RNE / PASS*
            <input id='rg_rne_pass' name='rg_rne_pass' required>
        </label>


        <label class='w5'>
            CPF*
            <input id='cpf' name='cpf' placeholder='123.456.789-10' required>
        </label>


        <label class='w6'>
            Empresa*
            <input id='empresa' name='empresa' required>
        </label>


        <label class='w6'>
            Profissão*
            <input id='profissao' name='profissao' required>
        </label>


        <label class='w6'>
            Tempo de Serviço*
            <input type='number' id='tempo_de_servico' name='tempo_de_servico' min='0' data-affixes-stay='true' data-prefix='' data-thousands='.' data-decimal='' placeholder='0' required>
        </label>


        <label class='w6'>
            Renda*
            <input id='renda' name='renda' data-affixes-stay='true' data-prefix='R$ ' data-thousands='.' data-decimal=',' placeholder='R$ 0,00' required>
        </label>


        <label class='w6'>
            FGTS*
            <input id='fgts' name='fgts' data-affixes-stay='true' data-prefix='R$ ' data-thousands='.' data-decimal=',' placeholder='R$ 0,00'>
        </label>


        <label class='w6'>
            Outras Rendas
            <input id='outras_rendas' name='outras_rendas' data-affixes-stay='true' data-prefix='R$ ' data-thousands='.' data-decimal=',' placeholder='R$ 0,00'>
        </label>


        <label class='w8'>
            Estado*
            <select name='uf' id='uf' placeholder='UF' required >
                <option></option>
                <option value='AC'>AC</option>
                <option value='AL'>AL</option>
                <option value='AP'>AP</option>
                <option value='AM'>AM</option>
                <option value='BA'>BA</option>
                <option value='CE'>CE</option>
                <option value='DF'>DF</option>
                <option value='ES'>ES</option>
                <option value='GO'>GO</option>
                <option value='MA'>MA</option>
                <option value='MT'>MT</option>
                <option value='MS'>MS</option>
                <option value='MG'>MG</option>
                <option value='PA'>PA</option>
                <option value='PB'>PB</option>
                <option value='PR'>PR</option>
                <option value='PE'>PE</option>
                <option value='PI'>PI</option>
                <option value='RJ'>RJ</option>
                <option value='RN'>RN</option>
                <option value='RS'>RS</option>
                <option value='RO'>RO</option>
                <option value='RR'>RR</option>
                <option value='SC'>SC</option>
                <option value='SP'>SP</option>
                <option value='SE'>SE</option>
                <option value='TO'>TO</option>
            </select>
        </label>

        <label class='w4'>
            Cidade*
            <select id='cidade' name='cidade' required>
                <option value=''></option>
                <option value=''>Selecionar Estado</option>
            </select>
        </label>

        <label class='w4'>
            Rua*
            <input id='rua' name='rua' required>
        </label>


        <label class='w8'>
            Número*
            <input id='numero' name='numero' required>
        </label>


        <label class='w4'>
            Bairro*
            <input id='bairro' name='bairro' required>
        </label>


        <label class='w3'>
            Telefone*
            <input id='telefone' name='telefone' placeholder='11-1234-5678' required>
        </label>


        <label class='w3'>
            Celular*
            <input id='celular' name='celular' placeholder='11-1234-56789' required>
        </label>


        <label class='w3'>
            Email*
            <input type='email' id='email' name='email' placeholder='nome@email.com' required>
        </label>

    </div>

    <label class='w1'>
        <input type='checkbox' id='segundo_comprador' name='segundo_comprador'>
        <span>Adicionar informações do 2º comprador</span>
    </label>

    <div id='cadastro_conjuge'></div>
        
    <br>
    <input type='submit' name='submit' class='submit' class='w1'>
</form>