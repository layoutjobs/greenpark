<?php
	extract($_POST);

	require 'sql-connect.php';

	$where = '';

	foreach ($_POST as $field => $value) {
		
		if ( $value != '' ) {

			if ( $where == '' ) {
				$where = $where . " where $field like '$value%' ";
			} else {
				$where = $where . " and $field like '$value%'";
			}

		} 

	}

	$sql = 'select * from tbl_cadastro'.$where.' order by id desc';
	$query = mysqli_query($con, $sql)or die($sql);

	while ( $rs = mysqli_fetch_array($query) ) {
		extract($rs);
		
		$nascimento = explode('-',$nascimento);

		$nascimento = $nascimento[2] .' / '. $nascimento[1] .' / '. $nascimento[0];

		$endereco = "$rua, nº $numero, $bairro, $cidade/$uf";

		$result = "
		<div class='row'>
			<div class='result'>
				<div class='inner-row'>
					<div class='w1'><h3>Dados do Primeiro Comprador</h3></div>
				</div>
			
				<div class='inner-row'>
					<div class='w2'> <b>Nome:</b> $nome</div>

					<div class='w8'> <b>Sexo:</b> $sexo</div>
					<div class='w8'> <b>Nascimento:</b> $nascimento</div>
					<div class='w8'> <b>Naturalidade:</b> $naturalidade</div>
					<div class='w8'> <b>Nacionalidade:</b> $nacionalidade</div>
				</div>

				<div class='inner-row'>
					<div class='w2'> <b>Endereço:</b> $endereco</div>
				</div>

				<div class='inner-row'>
					<div class='w8'> <b>Estado Civil:</b> $estado_civil</div>
					<div class='w8'> <b>Regime:</b> $regime_casamento</div>
					<div class='w8'> <b>RG / RNE / PASS:</b> $rg_rne_pass</div>
					<div class='w8'> <b>CPF:</b> $cpf</div>
					<div class='w6'> <b>Empresa:</b> $empresa</div>
					<div class='w6'> <b>Profissão:</b> $profissao</div>
					<div class='w6'> <b>Tempo de servico:</b> $tempo_de_servico</div>
				</div>

				<div class='inner-row'>
					<div class='w6'> <b>Renda:</b> $renda</div>
					<div class='w6'> <b>FGTS:</b> $fgts</div>
					<div class='w6'> <b>Outras Rendas:</b> $renda</div>
					<div class='w6'> <b>Telefone:</b> $telefone</div>
					<div class='w6'> <b>Celular:</b> $celular</div>
					<div class='w6'> <b>Email:</b> $email</div>
				</div>
		";

		if($segundo_comprador == 'on') {

			$sql = 'select * from tbl_cadastro_conjuge where cadastro_id = '.$id.' limit 1';
			$query_02 = mysqli_query($con, $sql)or die($sql);
			$rs = mysqli_fetch_array($query_02);
			extract($rs);
			$nascimento = explode('-',$nascimento);
			$nascimento = $nascimento[2] .' / '. $nascimento[1] .' / '. $nascimento[0];

			$result .= "
					<div class='inner-row'>
						<div class='w1'><h3>Dados do Segundo Comprador</h3></div>
					</div>

					<div class='inner-row'>
						<div class='w2'> <b>Nome:</b> $nome</div>
						<div class='w8'> <b>Sexo:</b> $sexo</div>
						<div class='w8'> <b>Nascimento:</b> $nascimento</div>
						<div class='w8'> <b>Naturalidade:</b> $naturalidade</div>
						<div class='w8'> <b>Nacionalidade:</b> $nacionalidade</div>
					</div>

					<div class='inner-row'>
						<div class='w6'> <b>RG / RNE / PASS:</b> $rg_rne_pass</div>
						<div class='w6'> <b>CPF:</b> $cpf</div>
						<div class='w4'> <b>Empresa:</b> $empresa</div>
						<div class='w4'> <b>Profissão:</b> $profissao</div>
						<div class='w6'> <b>Tempo de servico:</b> $tempo_de_servico</div>
					</div>

					<div class='inner-row'>
						<div class='w4'> <b>Renda:</b> $renda</div>
						<div class='w4'> <b>FGTS:</b> $fgts</div>
						<div class='w4'> <b>Outras Rendas:</b> $renda</div>
						<div class='w4'> <b>Celular:</b> $celular</div>
					</div>
			";
		}

		$result .= "
				</div>
			</div>
		";

		
		
		
		echo $result;
	}

	mysqli_close($con);
?>