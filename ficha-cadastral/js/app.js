$('#cpf').mask('999.999.999-99');
$('#celular').mask('99-9999-99999');
$('#telefone').mask('99-9999-9999');
$('#renda').maskMoney();
$('#fgts').maskMoney();
$('#outras_rendas').maskMoney();

document.querySelector('#uf').onchange = function() {
    data = 'uf='+this.value
    
    request = new XMLHttpRequest;
    request.open('POST', 'inc/sql-select-cities.php', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.send(data);

    request.onload = function() {
      
        if ( request.status >= 200 && request.status < 400 ) {

            response = request.responseText;

            document.querySelector('#cidade').innerHTML = response;

        }

    }
}

function hide_form(){
    document.querySelector('#cadastro_conjuge').innerHTML = '';
}

function show_form(){
    request = new XMLHttpRequest;
    request.open('POST', 'inc/form-cadastro-conjuge.php', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.send();

    request.onload = function() {
          if ( request.status >= 200 && request.status < 400 ) {
            document.querySelector('#cadastro_conjuge').innerHTML = request.responseText;
            $('#cpf_conjuge').mask('999.999.999-99');
            $('#celular_conjuge').mask('99-9999-99999');
            $('#telefone_conjuge').mask('99-9999-9999');
            $('#renda_conjuge').maskMoney();
            $('#fgts_conjuge').maskMoney();
            $('#outras_rendas_conjuge').maskMoney();
        }
    }
}

document.querySelector('#segundo_comprador').onchange = function() {

    this.checked == true ? show_form() : hide_form();

}

document.querySelector('.modal').onclick = function() {

    this.style.display = 'none'

}

document.querySelector('#estado_civil').onchange = function() {
    if(this.value == 'casado') {
        document.querySelector('#segundo_comprador').checked = true;
        document.querySelector('#regime_casamento').removeAttribute('disabled');
        show_form()
    } else{
        document.querySelector('#segundo_comprador').checked = false;
        document.querySelector('#regime_casamento').setAttribute('disabled', true);
        hide_form()
    }
}
