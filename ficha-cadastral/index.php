<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <title>Ficha Cadastral</title>
    <link rel='stylesheet' href='css/main.css'>
</head>

<body>
    <?php
        if ( isset($_GET['sent']) ) {

            if ( $_GET['sent'] == 'true' ) {
                echo "
                    <div class='modal' style='position: fixed; width: 100%; height: 100%; background: rgba(0,0,0,0.8); z-index: 99; top: 0; left: 0;'>
                        <p style='color: #009755; display: block; margin: 50px auto; width: 320px; background: #fff; text-align: center; padding: 20px 40px; border-radius: 5px; font: bold 18px tahoma'>Seu cadastro foi efetuado com sucesso, obrigado.</p>
                    </div>
                ";
            }

        }
    ?>

    <?php require 'inc/form-cadastro.php' ?>

    <script src='http://code.jquery.com/jquery-1.11.3.min.js'></script>
    <script src='js/masked-input.js'></script>
    <script src='js/mask-money.js'></script>
    <script src='js/app.js'></script>

    <style>
        select:invalid{
            /*outline: solid 1px green;*/
        }
    </style>
</body>
</html>